# proprietary_vendor_realme_firmware

Stock firmware images for a bunch of motorola devices, to include custom ROM builds.

### Supported devices
* Realme GT2 (porsche) : RMX3312_13.1.0.1240(EX01)

### How to use?

1. Clone this repo to `vendor/realme/firmware`

2. Inherit the appropriate firmware from `device.mk`, for example:

```
# Firmware
$(call inherit-product-if-exists, vendor/realme/firmware/porsche/BoardConfigVendor.mk)
```
